<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Activities Marked As Done</title>
</head>
<body>
	<h3>Activities Marked As Done</h3>
	<form action="http://localhost:8080/to-do-list/index.jsp" method="get"><input type="submit" value="Home"/></form>
	<br>
	<table border="2" width="30%" cellpadding="2">
        <tr>
            <th>ID</th>
            <th>List</th>
        </tr>
        <c:forEach items="${list}" var="task">
        	<c:if test="${task.is_done}">
	            <tr>
	                <td>${task.id}</td>
	                <td>${task.task}</td>
	             </tr>
             </c:if>
        </c:forEach>
    </table>
    <br>
    <form action="http://localhost:8080/to-do-list/index.jsp" method="get"><input type="submit" value="Home"/></form>

</body>
</html>