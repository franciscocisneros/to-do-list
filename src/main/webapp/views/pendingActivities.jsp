<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pending Activities</title>
</head>
<body>
	<h3>Pending Activities</h3>
	<form action="http://localhost:8080/to-do-list/index.jsp" method="get"><input type="submit" value="Home"/></form>
	<br>
	<table border="2" width="30%" cellpadding="2">
        <tr>
            <th>ID</th>
            <th>List</th>
            <th>Mark as Done</th>
        </tr>
        <c:forEach items="${list}" var="task">
            <c:if test="${!task.is_done}">
	            <tr>
	                <td>${task.id}</td>
	                <td>${task.task}</td>
	                <td><form action="../to-do-list/UpdateControllerExample" method="post"><input type="hidden" name= "id" value="${task.id}"/><input type="submit" value="Done"/></form></td>
	            </tr>
            </c:if>
        </c:forEach>
    </table> 
    <br>
	<form action="http://localhost:8080/to-do-list/index.jsp" method="get"><input type="submit" value="Home"/></form>
</body>
</html>