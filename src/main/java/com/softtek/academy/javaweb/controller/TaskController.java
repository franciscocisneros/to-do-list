package com.softtek.academy.javaweb.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.dao.TaskDao;
import com.softtek.academy.javaweb.model.TaskBean;

/**
 * Servlet implementation class Controller
 */
public class TaskController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("LISTA EN CONTROLLER: " + TaskDao.getAll());
		request.setAttribute("list", TaskDao.getAll());
		RequestDispatcher rd = request.getRequestDispatcher("views/pendingActivities.jsp");
        rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html"); 
		String task=request.getParameter("task");
	    TaskBean.postTask(task);
        request.getRequestDispatcher("/pendingActivities.jsp").forward(request, response);
	}

}
