package com.softtek.academy.javaweb.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.dao.TaskDao;
import com.softtek.academy.javaweb.model.TaskBean;

/**
 * Servlet implementation class UpdateController
 */
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("LISTA EN CONTROLLER: " + TaskDao.getAll());
		request.setAttribute("list", TaskDao.getAll());
		RequestDispatcher rd = request.getRequestDispatcher("views/doneActivities.jsp");
        rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html"); 
		int id= Integer.parseInt(request.getParameter("id"));
		System.out.println("ID a cambiar:"+id);
	    TaskBean.changeStatus(id);
        request.getRequestDispatcher("/doneActivities.jsp").forward(request, response);
	}

}
