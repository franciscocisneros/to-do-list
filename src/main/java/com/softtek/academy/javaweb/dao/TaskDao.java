package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.softtek.academy.javaweb.model.TaskBean;

public class TaskDao {
	public static Connection getConnection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/todolist?useUnicode=true&useJDBCComplieantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
			System.out.println("CONEXION EXITOSA");
		}catch(Exception e) {
			System.out.println(e);
		}
		System.out.println(con);
		return con;
	}
	public static ArrayList<TaskBean> getAll() {
		ArrayList<TaskBean> tasks = new ArrayList<TaskBean>();
		try {
            Connection connection = TaskDao.getConnection();
            PreparedStatement ps = connection.prepareStatement("SELECT * FROM to_do_list");
            ResultSet rs = ps.executeQuery();
           
            while(rs.next()) {
                TaskBean task = new TaskBean(rs.getInt("id"), rs.getString("list"), rs.getBoolean("is_done"));
                System.out.println("add task to list:" + task);
                tasks.add(task);
            }
            System.out.println("Array obtenido: "+tasks);
            return tasks;
           
        }catch(Exception e) {
            System.out.println(e);
            return tasks;
        }
    }
	public static void post(String task) {
		 try {
	            Connection connection = TaskDao.getConnection();
	            PreparedStatement ps = connection.prepareStatement("INSERT INTO `todolist`.`to_do_list` (`list`) VALUES (?);");
	            ps.setString(1, task);
	            ps.execute();
	           
	           
	        }catch(Exception e) {
	            System.out.println(e);
	        }
	        System.out.println(task);
	}
	public static void changeStatus(int id) {
		 try {
	            Connection connection = TaskDao.getConnection();
	            PreparedStatement ps = connection.prepareStatement("UPDATE `todolist`.`to_do_list` SET is_Done = 1 WHERE id=?;");
	            ps.setInt(1, id);
	            ps.execute();
	           
	        }catch(Exception e) {
	            System.out.println(e);
	        }
	}
	
	

}
