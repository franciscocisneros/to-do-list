package com.softtek.academy.javaweb.model;

import com.softtek.academy.javaweb.dao.TaskDao;
import com.softtek.academy.javaweb.model.TaskBean;

public class TaskBean {
	private int id;
	private  String task;
	private boolean is_done;

	
	public TaskBean(int id, String task, boolean is_done) {
		super();
		this.id = id;
		this.task = task;
		this.is_done = is_done;
	}

	
	public  String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}
	
	public static void postTask(String task){
		TaskDao.post(task);
	}
	
	public static void changeStatus(int id){
		TaskDao.changeStatus(id);
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public  boolean getIs_done() {
		return is_done;
	}

	public void setIs_done(boolean is_done) {
		this.is_done = is_done;
	}
	
	@Override
	public String toString() {
		return   "[id: " + id + " - name: " + task + " - is_done: " + is_done + "]";
	}
}
